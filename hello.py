import json
import boto3
from datetime import datetime 
import pandas as pd
import configparser
from sqlalchemy import create_engine
from sqlalchemy.types import Integer, Text, String, DateTime, Float



def lambda_handler(event, context):
    s3_client = boto3.client('s3')
    obj = s3_client.get_object(Bucket="cokebucket", Key="stock.json")
    serializedObject = obj['Body'].read()
    myData = json.loads(serializedObject)
    myData.append(str(datetime.now()))
    tempMap = {}
    tempMap["finalList"] = myData
    return myData
    
    
